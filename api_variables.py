cellType_list = ["leukocyte", "bone marrow hematopoietic cell", "cord blood hematopoietic stem cell", "glial cell",
                  "neuron", "peripheral blood mononuclear cell", "T cell", "epithelial cell of esophagus", "kidney cell",
                  "splenocyte", "endothelial cell of lymphatic vessel", "alpha-beta T cell", "cd4neg cd45pos immune cell",
                  "endodermal cell", "hematopoietic multipotent progenitor cell", "stromal cell",
                  "CD34-positive, CD38-negative hematopoietic stem cell", "endothelial cell of artery", "epithelial cell",
                  "CD45-", "hematopoietic stem cell", "natural killer cell", "somatic cell", "hematopoietic cell", "muscle cell",
                  "prostate stromal cell", "live", "mononuclear cell", "prostate epithelial cell", "memory T cell",
                  "bronchioalveolar stem cell", "epithelial cell of prostate", "subcutaneous fat cell", "Epcam+", "HLAG+",
                  "basal cell of prostate epithelium", "dendritic cell", "photoreceptor cell", "CAFs", "CD31+ endothelial",
                  "CD8-positive, alpha-beta T cell", "conventional dendritic cell", "innate lymphoid cell",
                  "luminal cell of prostate epithelium", "microglial cell", "myeloid cell",
                  "nucleus pulposus cell of intervertebral disc", "phagocyte", "plasma cell", "B cell",
                  "CD11b+ Macrophages/monocytes", "CD11b+CD11c+DC", "CD11c+ DC", "CD4+ T cell", "annulus pulposus cell",
                  "chondrocyte", "embryonic fibroblast", "fibroblast", "fibroblast of connective tissue of prostate",
                  "plasmacytoid dendritic cell", "smooth muscle cell of prostate", "Neoplastic cell", "acinar cell",
                  "astrocyte", "blastoderm cell", "common lymphoid progenitor", "dendritic cells", "early lymphoid progenitor",
                  "effector memory CD8-positive, alpha-beta T cell, terminally differentiated", "embryonic cell (metazoa)",
                  "embryonic stem cell", "epithelial cell of pancreas", "fetal liver hematopoietic progenitor cell",
                  "fibroblast of dermis", "granulocyte monocyte progenitor cell", "hepatocyte", "inhibitory interneuron",
                  "inner cell mass cell", "insulin secreting cell", "monocytes", "oligodendrocyte",
                  "oligodendrocyte precursor cell","pancreatic A cell", "pancreatic D cell", "pancreatic PP cell",
                  "pancreatic ductal cell", "pancreatic endocrine cell", "pre-conventional dendritic cell",
                  "vascular lymphangioblast"]
order_list = ["asc", "desc"]

donor_species_list = ["Homo sapiens", "Mus musculus"]

organ_list = ['blood', 'heart', 'brain', 'liver', 'immune system', 'lung', 'spleen', 'kidney', 'mouth', 'thymus',
              'esophagus','lymph node', 'large intestine', 'breast', 'decidua', 'zone of skin', 'prostate gland',
              'placenta', 'endoderm', 'embryo', 'blood vessel', 'bone marrow', 'colon', 'arterial blood vessel',
              'digestive system', 'eye', 'hematopoietic system', 'immune organ', 'skeletal muscle organ', 'yolk sac',
              'retina', 'pancreas','adipose tissue', 'mediastinal lymph node', 'testis', 'whole embryos',
              'intervertebral disk','bone element','skeletal element', 'skin of body', 'abdomen', 'adrenal gland',
              'blastocyst', 'cerebral cortex', 'intestine','muscle tissue', 'skin', 'stomach', 'striatum', 'tail',
              'tonsil', 'tumor', 'umbilical cord','wall of lateral ventricle']

sort_list = ['accessions', 'aggregateLastModifiedDate', 'aggregateSubmissionDate', 'aggregateUpdateDate',
             'assayType', 'biologicalSex', 'bundleUuid', 'bundleVersion', 'cellCount', 'cellLineType',
             'contactName', 'contentDescription', 'developmentStage', 'donorCount', 'donorDisease',
             'effectiveCellCount', 'effectiveOrgan', 'entryId', 'fileFormat', 'fileId', 'fileName',
             'fileSize', 'fileSource', 'fileVersion', 'genusSpecies', 'institution', 'instrumentManufacturerModel',
             'isIntermediate', 'laboratory', 'lastModifiedDate', 'libraryConstructionApproach',
             'matrixCellCount', 'modelOrgan', 'modelOrganPart', 'nucleicAcidSource', 'organ', 'organPart',
             'organismAge', 'organismAgeRange', 'pairedEnd', 'preservationMethod', 'project',
             'projectDescription', 'projectEstimatedCellCount', 'projectId', 'projectTitle',
             'publicationTitle', 'sampleDisease', 'sampleEntityType', 'sampleId', 'selectedCellType',
             'sourceId', 'sourceSpec', 'specimenDisease', 'specimenOrgan', 'specimenOrganPart',
             'submissionDate', 'updateDate', 'workflow']

sampleDisease_list = ["normal", "multiple sclerosis", "tongue cancer", "COVID-19", "pulmonary fibrosis",
              "thoracic aortic aneurysm", "human papilloma virus infection", "non-alcoholic fatty liver disease",
              "Alzheimer disease", "ulcerative colitis (disease)", "atherosclerosis", "cerebral amyloid angiopathy",
              "influenza", "HIV infectious disease", "alcoholic fatty liver disease", "carcinoma of floor of mouth",
              "colitis (disease)", "tonsil carcinoma", "glaucoma (disease)", "oral cavity carcinoma",
              "plasma cell myeloma", "squamous cell carcinoma of buccal mucosa", "carcinoma of supraglottis",
              "cirrhosis of liver", "cognitive impairment with or without cerebellar ataxia",
              "hereditary hemochromatosis", "laryngeal carcinoma", "lower gum cancer", "mandibular cancer",
              "orofaciodigital syndrome VIII", "primary biliary cholangitis", "atypical chronic myeloid leukemia",
              "glioblastoma (disease)", "type 1 diabetes mellitus", "type 2 diabetes mellitus"]

welcomeText = """This skript will provide a selection of loom-files from the Human Cell Atlas Database
depending on your search criteria 
In July 2023 there are 1168 loom-files saved in Human Cell Atlas. \n"""

filtervariablesText = """Please specify which files you are looking for. For every filter-variable you can type in:
   * a single string
   * multiple strings, separated by a space followed by a comma
   * nothing (just press enter)
if you want to see possible inputs type in a question mark, this option is not available for ProjectID and ProjectTitle
don't use quotation marks ! \n"""


hits_order_Text = """\n The following variables determine the sort criteria, order and number of hits 
to display or download. If you do not want to adjust them, press enter.\n"""


