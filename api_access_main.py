from api_methods import *

"""This script offers access to file related metadata and the opportunity to 
download loom-files from Human Cell Atlas There are two use cases defined:

    * interactive through console input: after specifing the filter-criterias 
    for loom-files in the console, a table containing meta-data of matching 
    loom-files will be printed out. Every file gets a index and the user will 
    be asked to type in the indices of files, which he  want's to download. 
    Files will be downloaded automatically to ./data if no alternative path 
    is provided This use case gives the possibility to have a look at the 
    files meta-data and decide afterwards which files to download. start by 
    calling: usecase1_interactive()

    * Non-Interactive Script: After modifying the dictionary to specify the 
    filter criteria for Loom files, as well as the directory path where the 
    files should be saved, the data is downloaded automatic. usecase2 
    produces logs stored in a logs.log file. start by calling: usecase2()"""

####################################################
#         usecase 1 (interactive):                 #
####################################################

# usecase1_interactive()


####################################################
#         usecase 2 non interactive:               #
####################################################

# Specify Parameters: The provided parameters are default values;
# feel free to modify them according to your needs.
# the params (except for the first three)
# can be single strings or lists of strings
# e.g. organ = ['heart','blood'] or organ = 'thymus'

filter_variables = dict(numberFiles=5,
                        sort='lastModifiedDate',
                        order='asc',
                        projectId=False,
                        projectTitle=False,
                        organ=False,
                        donor_species="Homo sapiens",
                        sampleDisease="normal",
                        cellType=False)

# save_location (default ./data )
cwd = os.getcwd()
save_location = os.path.join(cwd, 'data')

# check if folder data exists in save_location and create ./data if not
if not os.path.exists(save_location):
    os.makedirs(save_location)

# download data:
usecase2(filter_variables, save_location)
