from api_access_main import download_looms_by_organ_list
from data_sampling import get_list_of_loom_files

#######################
# SETTINGS            #
#######################
organ_list = ['heart', 'blood', 'lung', 'brain', 'kidney', 'eye', 'liver']
nr_files_per_organ = 1
species = "Homo sapiens"

#######################
# DOWNLOAD FILES      #
#######################
download_looms_by_organ_list(organ_lst=organ_list,
                             nr_files=nr_files_per_organ,
                             species=species)
print("Now there are the following loom files locally:",
      *get_list_of_loom_files(), sep="\n")
