from sys import getsizeof
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from data_cleaning import *
from data_sampling import save_df
from tsne_pca_methods import tsne_perplexities, plot_data, plot_six_plots, pca_on_ac_input
from autoencoder_methods import *

###################################################
##            data import and preprocessing     ###
###################################################

# Reading the data and labels from CSV files
dataset = pd.read_csv("./kaggle_data/data/data/data.csv")
labels_df = pd.read_csv("./kaggle_data/data/data/labels.csv")
df = pd.DataFrame(dataset)
labels = labels_df['Class'].tolist()
# drop a column which contains strings:
df_drop = df.drop('Unnamed: 0', axis=1)

df_clean = preprocessing(df_drop, scaling='normalize')

# add labels for splitting data in test and train:
df_clean['label'] = labels
save_df(df_clean, name="clean_kaggle_df", directory="kaggle_data")
print(f"Latent_space df has size: {getsizeof(df_clean) / 1000000.} MB")

# Get train and test set
x_train, x_test = train_test_split(df_clean, test_size=0.3,
                                   random_state=42)
print(f"DF x_train has size: {getsizeof(x_train) / 1000000.} MB")
print(f"DF x_test has size: {getsizeof(x_test) / 1000000.} MB")

# drop labels again and save them in separate lists:
train_labels = x_train['label']
test_labels = x_test['label']
x_train = x_train.drop('label', axis=1)
x_test = x_test.drop('label', axis=1)

######################################
##             PCA                ####
######################################

transformed_test_data = pca_on_ac_input(x_train=x_train,x_test=x_test)
# print("type transformed data: ", type(transformed_test_data))

########################################################
##          TSNE ON FIRST 16 PRINCIPAL COMPONENTS    ###
#########################################################


## Tsne for perplexity = 30
sing_res_tsne_pca = tsne_perplexities(perplexities=[30], df=transformed_test_data)

"""
## Tsne for different perplexities
perplexities = [10,20,40,60,80,100]
multi_res_tsne_pca = tsne_perplexities(perplexities=perplexities, df=principal_comp)
"""
# plot results
title1 = "Result T-sne after PCA on Autoencoder_input, with perplexity 30  (kaggle data)"
plot_data(sing_res_tsne_pca, test_labels, title1)

""" 
title2 = "tsne on pca results, different perplexities (kaggle data)"
plot_six_plots(results = multi_res_tsne_pca,
               labels = labels,
               param_list=perplexities,
               param_name="perplexity",
               title = title2)
"""

## Tsne for perplexity = 30
sing_res_tsne_pca = tsne_perplexities(perplexities=[30], df=transformed_test_data)

"""
## Tsne for different perplexities
perplexities = [10,20,40,60,80,100]
multi_res_tsne_pca = tsne_perplexities(perplexities=perplexities, df=principal_comp)
"""
# plot results
title1 = "Result T-sne after PCA on Autoencoder_input, with perplexity 30  (kaggle data)"
plot_data(sing_res_tsne_pca, test_labels, title1)

""" 
title2 = "tsne on pca results, different perplexities (kaggle data)"
plot_six_plots(results = multi_res_tsne_pca,
               labels = labels,
               param_list=perplexities,
               param_name="perplexity",
               title = title2)
"""














