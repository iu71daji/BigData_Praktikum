from data_sampling import *
from sys import getsizeof
from data_cleaning import preprocessing
from sklearn.model_selection import train_test_split

#######################
# SETTINGS            #
#######################
file_name_ending = "mid"
nr_cols = 2048
ac_layer_dims = (128, 16, 128)
# For splitting
test_size = 0.3
random_state = 42

###################################
# PREPROCESSING & SCALING         #
###################################

# Get a dataframe
df = load_df(f"labeled_{file_name_ending}")
print(df)
print(f"DF has the size: {getsizeof(df) / 1000000.} MB")


# Cleaning & scaling
df = preprocessing(df, scaling="normalize", nr_cols=nr_cols)
print("DF after preprocessing:\n", df)

# Save the DataFrame
save_df(df, name=f"clean_test_df_{file_name_ending}")

#######################################
# Split into training and test data   #
#######################################

x_train, x_test = train_test_split(df, test_size=test_size,
                                   random_state=random_state)
print("Training set:\n", x_train)
save_df(df, name=f"x_train_{file_name_ending}")

print("Test set:\n", x_test)
save_df(df, name=f"x_test_{file_name_ending}")
