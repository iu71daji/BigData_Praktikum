#!/bin/bash
#SBATCH --job-name=get_looms_for_autoencoder
#SBATCH --output=BigData_Praktikum/slurm_%a_%j.out
#SBATCH --error=BigData_Praktikum/slurm_%a_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=clara
#SBATCH --time=2:00:00

# set for modin parallelization
export __MODIN_AUTOIMPORT_PANDAS_=:1

cd BigData_Praktikum/
source venv_autoencoder/bin/activate
python main_download_looms.py RUN_ID=$1
