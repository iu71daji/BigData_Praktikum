from data_sampling import *
from sys import getsizeof

#######################
# SETTINGS            #
#######################
file_name_ending = "mid"
nr_cols = 12000
nr_rows = 900

###############################################
# TEST FILES                                  #
###############################################
looms = [x for x in get_list_of_loom_files() if "__" in x]
print(looms)

###############################################
# CREATE DATAFRAME and save as PICKLE         #
###############################################

print(f"Create DataFrame with {nr_cols} features and {nr_rows} rows per file")
data, o_lbls, a_lbls = sample_of_several_looms(looms,
                                               nr_cols=nr_cols,
                                               nr_rows=nr_rows,
                                               check_attrs=False)
df = DataFrame(data, index=o_lbls, columns=a_lbls)


print("DF:\n", df)

with open(f'testfiles/labeled_{file_name_ending}.pkl', 'wb') as outp:
    pickle.dump(df, outp, pickle.HIGHEST_PROTOCOL)

print("DF created and saved with pickle as:\n",
      f"testfiles/labeled_{file_name_ending}.pkl\n")
print(f"DF has the size: {getsizeof(df) / 1000000.} MB")

print(get_data_labels(df))


"""
###############################################
# TEST Function `loom_to_pandas_df(file_name)`#
###############################################

test_df = loom_to_pandas_df(file_name=mouse, limits=(0, 15500))
test_df.info()
test_df.describe()
print(f"\nShape of the DataFrame: {test_df.shape}")
print(test_df)

print("\nNORMALIZE")
# test_df = test_df.div(test_df.sum(axis=0), axis=1)
for col in test_df.columns:
    test_df[col] = test_df[col] / test_df[col].abs().max()
print(test_df)
"""

"""
###############################################
# TEST other functions                        #
###############################################
from other_methods import *

# TEST Function `inspect_loom_file(file_name)`#
inspect_loom_file(looms[0])
# inspect_loom_file(f1)  # shape: (58347, 218458)
# inspect_loom_file(f2)  # shape: (58347, 132625)
# inspect_loom_file(f3)  # shape: (58347, 267690)

# TEST Function `sample_of_several_looms(file_name)`#
a, b, c = sample_of_several_looms([looms[0], looms[1]],
                                  nr_cols=10, check_attrs=True, nr_rows=10)
print(a, b, c, sep="\n")
# TEST Function `random_np_sample(file_name)`#
a = random_np_sample(a, nr_cols=2)

"""


"""
# TEST Function `sample_df_of_looms(file_name)`#
df = sample_df_of_looms([f1, f3], nr_cols=5000, nr_rows=1000)
print(df)

#############################################
#       APPLY AUTOENCODER ON df             #
#############################################

# TODO: Paul fragen: df hat dann die dims (x,10) also es wurden nur 10 
Spalten = Gene verwendet aber alle Zellen, # müsste es nich andersrum sein? 
oder sind die Zeilen = Gene ? Müssen wir es nich dann drehen für den 
Autoencoder Input? # preprocessing # für split in test & tain müssen wir dann 
entsprechend entlang der Zellen splitten, also einen Zeil Zellen in Train und 
# einen kleineren in Test #

df_norm = preprocessing(df, scaling="standardize")
print("dimensionen des df: ",df_norm.shape)
# split into train and test
X_train, X_test = train_test_split(df_norm, test_size=0.3, random_state=42)

print("X_train dims :", X_train.shape)
print("X_test dims: ",X_test.shape)

# define model
# ac = define_autoencoder(df_norm)

# train model
# history, autoencoder = train_autoencoder(ac, X_train, X_test)

# print(history)
"""
