from data_cleaning import *
from tsne_pca_methods import *
from data_cleaning import *
from data_sampling import *
"""
for method comparison we perform a pca on the data which we used for the autoencoder. We take the first 
 16 principal components and perform  T-sne on them. Comparing the results will show which method (PCA or 
 autoencoder performs better in reducing the feature dimensions to 16. 
"""

##################################################
##         data import and preprocessing        ##
##################################################


# Get dataframe ( same which goes into autoencoder, already preprocessed (normalized))
# everything together, no split in test and train necessary.
df = load_df(f"clean_test_df_mid")
print(df)
# print(f"DF has the size: {getsizeof(df) / 1000000.} MB")
print(df.describe)

# get lables:
labels = get_data_labels(df)



# get down to 16 dimensions with pca
principal_comp = pca_on_ac_input(df)

## Tsne for perplexity = 30
sing_res_tsne_pca = tsne_perplexities(perplexities=[30], df=principal_comp)

## Tsne for different perplexities
perplexities = [10,20,40,60,80,100]
multi_res_tsne_pca = tsne_perplexities(perplexities=perplexities, df=principal_comp)

# plot results
title1 = "Result T-sne after PCA on Autoencoder_input, with perplexity 30 (Human cell atlas data)"
plot_data(sing_res_tsne_pca, labels, title1)

title2 = "tsne on pca results, different perplexities (Human cell atlas data)"
plot_six_plots(results = multi_res_tsne_pca,
               lables = labels,
               param_list=perplexities,
               param_name="perplexity",
               title = title2)