import os
from pandas import DataFrame
import main_methods as mm
import time
from loompy import connect
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
import numpy as np


#####################
# Some SPEED tests  #
#####################

def speed_test_np_vs_pd(number_of_cols=5000,
                        file="afd0ea55-e710-4b46-bb05-2423e491b6f5.loom"):
    """
    Numpy is usually faster than Pandas
    but on reading in loom files it seems to be different.
    """
    # 1. NUMPY #
    start = time.time()
    with connect(f"data/{file}") as ds:
        attribute_labels = ds.ca.CellID
        object_labels = ds.ra.Gene
        data = ds[:number_of_cols, :]
        print("type:", type(data))
        print(data.shape)
    end = time.time()
    print("To Numpy took:", end - start)

    # 2. PANDAS #
    start = time.time()
    df = mm.loom_to_pandas_df(file, limits=(0, number_of_cols))
    print("type:", type(df))
    print(df.shape)
    end = time.time()
    print("To Pandas DF took:", end - start)
    return df


def speed_test_scaling(df, number_of_cols=5000, scaling="standardize"):
    df.dropna(how='any', axis=1)
    # 1. SK-Learn #
    start = time.time()
    scaler = StandardScaler()
    scaler.fit(df)
    scaled = scaler.fit_transform(df)
    end = time.time()
    print("SK-Learn took:", end - start)
    print(np.mean(scaled))

    # 2. PANDAS #
    start = time.time()
    scaled_2 = mm.standardize_data(df)
    end = time.time()
    print("Pandas took:", end - start)
    print(np.mean(scaled_2))



df = speed_test_np_vs_pd(number_of_cols=4000)
df.info(memory_usage='deep')
speed_test_scaling(df)
