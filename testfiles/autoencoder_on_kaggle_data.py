from sys import getsizeof

import pandas as pd
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split

from data_cleaning import *
from data_sampling import save_df
from tsne_pca_methods import tsne_perplexities, plot_data, plot_six_plots, pca_on_ac_input
from autoencoder_methods import *

###################################################
##            data import and preprocessing     ###
###################################################

# Reading the data and labels from CSV files
dataset = pd.read_csv("./kaggle_data/data/data/data.csv")
labels_df = pd.read_csv("./kaggle_data/data/data/labels.csv")
df = pd.DataFrame(dataset)
labels = labels_df['Class'].tolist()
# drop a column which contains strings:
df = df.drop('Unnamed: 0', axis=1)
# add column 'labels'
print("df vor prepro: ")
print(df)

df_clean = preprocessing(df, scaling='normalize', nr_cols=2048)
print("df clean: ")
print(df_clean)


# add labels for splitting data in test and train:
df_clean['label'] = labels

# Get train and test set
x_train, x_test = train_test_split(df_clean, test_size=0.3,
                                   random_state=42)
print(f"DF x_train has size: {getsizeof(x_train) / 1000000.} MB")
print(f"DF x_test has size: {getsizeof(x_test) / 1000000.} MB")

# drop labels again and save them in seperate lists:
train_labels = x_train['label']
test_labels = x_test['label']
x_train = x_train.drop('label', axis=1)
x_test = x_test.drop('label', axis=1)


##########################################
##          Autoencoder                ###
##########################################

# Settings
epochs = 90
batch_size = 256
ac_input_dim = 2048
ac_layer_dims = (128, 16, 128)


######################################################
#       APPLY AUTOENCODER ON KAGGLE DATA            ##
######################################################


# Define model
autoencoder, encoder, decoder = define_autoencoder(x_train.shape[1],
                                                   ac_layer_dims)

# Compile the autoencoder model
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')

# Train the model
history = autoencoder.fit(x_train, x_train,
                          epochs=epochs,
                          batch_size=batch_size,
                          shuffle=True,
                          validation_data=(x_test, x_test))

# Save models
autoencoder.save(f"autoencoder_model_kaggle.keras")
decoder.save(f"decoder_model_kaggle.keras")

# Plot
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# Apply model
latent_space = DataFrame(encoder.predict(x_test))
# Save the latent_space as pandas DataFrame
save_df(latent_space, name=f"latent_space_kaggle")
print(f"Latent_space df has size: {getsizeof(latent_space) / 1000000.} MB")

print("latentspace : ")
print(latent_space)


###############################################
##             TSNE ON LATENTSPACE           ##
###############################################

## Tsne for perplexity = 20
sing_res_tsne_pca = tsne_perplexities(perplexities=[20], df=latent_space)
print("tsne result: ", sing_res_tsne_pca)

""" 
## Tsne for different perplexities
perplexities = [10,20,40,60,80,100]
multi_res_tsne_pca = tsne_perplexities(perplexities=perplexities, df=latent_space)
"""
# plot results
title1 = "Result T-sne on latent space, with perplexity 20  (kaggle data)"
print("len lables: ", len(labels))
plot_data(results=sing_res_tsne_pca, labels=test_labels, title=title1)
""" 
title2 = "tsne on pca results, different perplexities (kaggle data)"
plot_six_plots(results = multi_res_tsne_pca,
               labels = labels,
               param_list=perplexities,
               param_name="perplexity",
               title = title2)
## tsne on latentspace:
perplexities = [10,20,40,60,80,100]
"""




