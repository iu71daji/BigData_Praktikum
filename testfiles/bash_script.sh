#!/bin/bash

python_version="3.8.10"
venv_name="venv_autoencoder"

# Create the virtual environment
python${python_version} -m venv ${venv_name}

# Activate the virtual environment
source ${venv_name}/bin/activate

# Upgrade pip
pip install --upgrade pip
# install requirements recursivly
pip install -r requirements.txt

echo "Python virtual environment '${venv_name}' created successfully."

python test_main_methods.py

