from data_sampling import *
from sys import getsizeof
from autoencoder_methods import *
from matplotlib import pyplot as plt


#######################
# SETTINGS            #
#######################
file_name_ending = "mid"
epochs = 120
batch_size = 256
ac_input_dim = 2048
ac_layer_dims = (128, 16, 128)


#############################################
#       APPLY AUTOENCODER ON df             #
#############################################

# Get train and test set
x_train = load_df(f"x_train_{file_name_ending}")
x_test = load_df(f"x_test_{file_name_ending}")
print(f"DF x_train has size: {getsizeof(x_train) / 1000000.} MB")
print(f"DF x_test has size: {getsizeof(x_test) / 1000000.} MB")

# Define model
autoencoder, encoder, decoder = define_autoencoder(x_train.shape[1],
                                                   ac_layer_dims)

# Compile the autoencoder model
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')

# Train the model
history = autoencoder.fit(x_train, x_train,
                          epochs=epochs,
                          batch_size=batch_size,
                          shuffle=True,
                          validation_data=(x_test, x_test))

# Save models
autoencoder.save(f"autoencoder_model_{file_name_ending}.keras")
decoder.save(f"decoder_model_{file_name_ending}.keras")

# Plot
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# Apply model
latent_space = DataFrame(encoder.predict(x_test))
# Save the latent_space as pandas DataFrame
save_df(latent_space, name=f"latent_space_{file_name_ending}")
print(f"Latent_space df has size: {getsizeof(latent_space) / 1000000.} MB")
print(latent_space)
