#Importing required libraries

import pandas as pd
import matplotlib.pyplot as plt

# Reading the data and labels from CSV files
dataset = pd.read_csv("./kaggle_data/data/data/data.csv")
lb = pd.read_csv("./kaggle_data/data/data/labels.csv")

# Merging the dataset and labels into a single dataframe
df = pd.merge(lb,dataset)
df.head()

# Checking the number of null values in each column
df.isnull().sum()

# Generating descriptive statistics that summarize the central tendency, dispersion, and shape of a dataset’s distribution
df.describe()

# removes the 'Unnamed: 0' and 'Class' columns from the dataframe
df_tsne_data = df
non_num = ['Unnamed: 0','Class']
df_tsne_data = df_tsne_data.drop(non_num, axis=1)
df_tsne_data

# Checking if the columns are numerical or not
for col in df_tsne_data.columns:
    try:
        _ = df_tsne_data[col].astype(float)
        print(f'Column {col} is numeric')
    except ValueError:
        print(f'Column {col} is not numeric')

# import t-SNE from sklearn
from sklearn.manifold import TSNE
m = TSNE(learning_rate=1, perplexity=35)

# t-SNE features are computed and a subset is printed
tnse_features = m.fit_transform(df_tsne_data)
print( "tsne_features : ", tnse_features[1:6,:])

#Two new columns 'x' and 'y' are added to the dataframe. They correspond to the t-SNE features.
df_tsne_data['x'] = tnse_features[:,0]
df_tsne_data['y'] = tnse_features[:,1]
# plots the t-SNE features
import seaborn as sns
sns.scatterplot(x='x',y='y',data=df_tsne_data)
plt.show()

df['Class']

# The 'Class' column is added back to the dataframe under a new name 'cancer'
df_tsne_data['cancer']=df['Class']

# The t-SNE features are plotted again, but this time the points are colored based on the 'cancer' class
sns.scatterplot(x='x',y='y',hue = 'cancer', data= df_tsne_data)
plt.show()

