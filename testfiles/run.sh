#!/bin/bash
#SBATCH --job-name=autoencoder
#SBATCH --output=BigData_Praktikum/slurm_%a_%j.out
#SBATCH --error=BigData_Praktikum/slurm_%a_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mem=32G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --gres=gpu:rtx2080ti:1
#SBATCH --partition=clara
#SBATCH --time=2:00:00

ml cuDNN
# set for cuda reproducibility
export CUBLAS_WORKSPACE_CONFIG=:4096
# set for modin parallelization
export __MODIN_AUTOIMPORT_PANDAS_=:1

cd BigData_Praktikum/
source venv_autoencoder/bin/activate
python main.py RUN_ID=$1
