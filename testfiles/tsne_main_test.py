from tsne_pca_methods import *
from data_sampling import *
from data_cleaning import *
# load data frame, already preprocessed ( if not do so and latent space does not need
# and further processing i guess.

df = load_df(f"latent_space_mid")
print(df)
# print(f"DF has the size: {getsizeof(df) / 1000000.} MB")
print(df.describe)

# if latentspace -> original dataset needed to get the labels because latentspace is not labled
# but rows are unchanged TODO: Annahme prüfen.
df_original = load_df(f"labeled_mid")
labels = get_data_labels(df_original)


## Tsne for different perplexities
# perplexities = [10,20,40,60,80,100]
perplexities = [40]
res_perp = tsne_perplexities(perplexities, df)
title = "t-sne on latentspace"
# if you have one single perplexity vlalue
plot_data(res_perp, labels)

# if you have more ( not more than 6 and 6 would be perfect):
""" 
plot_six_plots(results = res_perp,
               labels = labels,
               param_list=perplexities,
               param_name="perplexity",
               title = title)
"""


# different learingrates
""" 
learningrate = [1,10,100,300,550,800]
res_learn = tsne_learningrates(learningrate,df)
title = "t-sne on latentspace, perplexity 40, different learningrates"
plot_six_plots(results=res_learn,
               labels=labels,
               param_list=learningrate,
               param_name="learningrate",
               title=title)

"""



###################################################
##              trash                           ###
###################################################

#Two new columns 'x' and 'y' are added to the dataframe. They correspond to the t-SNE features.
# df_clean['x'] = tnse_features[:,0]
# df_clean['y'] = tnse_features[:,1]



# Notizen zu TSNE Funktion
# learningrate [10, 1000] variieren
# n_iter default=1000