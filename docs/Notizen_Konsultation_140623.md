# Notizen Konsultation Maximilian & Jan 
14.06.23
### 1. API
* Projekt-ID 
* Paramter-ID, dann die ersten 100 oder 1000 files anzeigen lassen. Filtern nach Project-ID, thema "Krebs", "Gewebetyp"
* was sin die Projekte, Projekt-IDs 
* use-case: alle loom-files für Daten über Mensch nehmen. 
    auf HPC Cluster dann z.B. Autoencoder 
   
### 2. Autoencoder
* Keras is einfacher, die beiden selbst machen das mit Pythorch und können uns da auch helfen (Max)
    im wissenschaftlichen Bereich und allgemein scheint Pythorch state of the art; sich etwas durchzusetzen.
* wir können ne eigene Dataloader-Klasse schreiben wenn wir extra kramsi brauchen, können eigentlich aber auch das mit der pythorch-Dataloader-Klasse arbeiten. können uns hilfe geben lassen. 
* funktioniert Autoencoder gut ? -> latent space visualisiert anschauen; biologische Auftrennung muss wieder zu finden sein im latent-space. Also das auch als Qualitätskontrolle. Z.B. Unterscheidung Haut und Hirn zellen müssten sich stark unterscheiden. 
* testdaten: nich an den zellen sondern an den features sparen -> also gene rauspicken; die mit der höheren Varianz.
* preprocessing: wie varianz treshhold wählen? var = 0 rausschmeißen auf jeden fall. 
    die machen das so: top 1000 oder top 100 wählen -> also höchste Varianz nahmen und selbst bestimmen wie viele Features genommen werden. 
    ansich is auch beides interessant, könntne wir uns mal anschauen, nich dass am ende die letzten Zahlen var = 0 haben. Aber top 1000 z.B. ist auch voll oke für max & jan. 
    treshhold nich perse falsch, verteilung anschauen. 
  * alle daten müssen den gleichen feature-satz haben. Also Zellen haben halt die gleichen Gene 
  * NaN-Values: ganzes Gen droppen bei NaN-Value in einem Gen? -> Jan & Max haben auch kein perse perfekte Lösung. Frage ist: drop feature or drop sample (=zelle)
    Weil wir so viel Features haben liegt es nahe Features zu droppen. 
    nullen einsetzen nich, eher einen mean, weil 0 ist ja ein extremwert bei uns. oder mode (was is mode ?)
    loggen was wir rausschmeißen
    * schauen ob samples scheiße sind -> wenn ein sample 90% NaNs hat, is scheiße. dann drop sample. 
    * so 30 % nans is schon viel in so nem Datensatz 
    * also check einbauen: wie viel kleiner wird Datensatz wenn ich alle nan features des Samples lösche 
  ### Autoencoder validierung: Wann is unser modell gut ? 
* loss-function: ab wann ist das ergebnis gut? schauen ob wir nur ein lokales oder maximales minimum errreicht haben. lässt sich nich sagen ab wann die Werte sinnvoll sind, 
* schauen ob ergebnisse biologisch sinn ergeben.
* statt autoencoder mal ne pca oder u-map auf feature-map (also autoencoder-input) machen. Schauen, Autoencoder soll besser sein, also mind so gut wie pca, besser eigentlich als sanety-check. also nur 2 Hauptcomponenten nehmen. Dann vergleichen mit erst autoencoder dann t-sna / u-map oder so. 
* theoretisch auch möglich latentspace auf 2 setzen, also 2 dimensionen. is normalerweise aber nich gut im ergebnis.
* lossverlauf anschauen, wie viele epochen müssen gerechnet werden ?  

* professionelles Hyperparameter-Tuning: ist quasi systemmatisches ausprobieren; Optimierungsverfahren. 

### weitere Fragen: 
* Latentspace enthält oft viele Nullen: Problem wird sich eigentlich lösen, wenn wir realistischere und größere Datenmengen haben. Bei Max & Jan kommt das so nich vor in den Implementierungen. 
* wie viele Layer sollen wir machen ? zwischen 2 und 5 wäre ne Idee, 2 is eher wenig, 5 sind eher viel. VIelleicht mal mit 3 Layern starten. 
* an Literatur orientieren oder tunen. An Literatur orientieren ist auch oke. Tuning is cool und spannend, aber relativ kompliziert. Also Tuning meint hier quasi systemmatisch ausprobieren. 
* fullskill hyperparameter-tuning zu trainieren wäre ganzschön aufwendig, so nach Literatur machen wäre gut, in jedem Layer die anzahl der feature inputs halbieren circa
### synthetische Daten erstellen evtl. 
* dataframe mit von uns definierter anzahl an clustern (scitit-learn)

*Benotung: is vermutlich auch die Lösungsskizze, Präsi, Code.. alles :) *
Präsi wird von Rahm bewertet. 

### scientific cluster: 
* wie sollen wir damit arbeiten? venv python sollte ausreichen. 
* loompy bug dokumentieren
* wenn wir an grenzen unserer pc's stoßen sollten wir aufs cluster gehen 
* wir sollten das auch mal auf dem cluster versuchen vor abgabe 
* wie auf cluster arbeiten 
    * jobs losschicken, mit vs-code verbinden aufs cluster (oder bim)
    * dann losschicken über slearm dann losschicken 
    * auf sc uni leipzig seite gibts dazu ein tutorial. 
    * z.B. ein bash-skript machen, dann slearm config da rein laden, dann die skripte ausführen lassen von dem bash-skript
    * nich auf der login- dingis machen, wenn wir da viel ressourcen brauchen werden wir gedrosselt :D 
    * bash-skript als slearm job starten.. 
    * Modul-system: alle gängigen Modulsysteme gibt es und wir müssen sie in unseren workspace rein laden. Befehl ml für modul-load 
    * losschicken mit sbatch run.sh
    * VS Code, remote extension damit können wir uns mit dem Cluster verbinden. 
    * über terminal tmux verwenden. (googeln was das macht.. ?)
    * code über git aufs cluster laden und dann daten mit ? naja für uns über api 
    * sbatch soll man immer von home aus starten, also cd in home. 
    * job hat ne id und wir können den job dann beoobachten. oder in die logfiles reinschauen.. 
    * login-nodes sind manchmal down, es gibt ein paar, dann einfach einen andern verwenden login1 / login2 … 
    * TODO  also wärs für uns gut logging-befehle in den code zu bauen. 
    * workflow: erstmal schauen dass es ohne buggs lokal läuft, dann auf dem cluster laufen lassen
        debugging is auf dem cluster nerviger.. 
        10 000 Samples und 500 - 10 000 features geht auch meist lokal klar.. 
        
        viktor christen schreiben wegen präsi termin. 
        
    ### CHAT LINKS: Maximilian Josef Joas an Alle (14. Juni 2023, 14:20)  
    
* https://www.assemblyai.com/blog/pytorch-vs-tensorflow-in-2023/  

* https://www.frontiersin.org/articles/10.3389/fgene.2019.01205/full  

* https://www.mdpi.com/2072-6694/13/9/2013

* https://scikit-learn.org/stable/datasets/sample_generators.html#sample-generators  

* https://scikit-learn.org/stable/modules/generated/sklearn.datasets.make_blobs.html#sklearn.datasets.make_blobs

### t-sne kramsi übrigens: 
by the way t-sne kramsi: https://distill.pub/2016/misread-tsne/
Notizen zum Text: 
    * tsne-results können fehlinterpretiert werden.
    * nicht learer Algorithmus. Je nach Region werden unterschiedliche Transformationen ausgeführt
    * perplexity: wie fokus auf lokale und globale aspekte der Daten balanciieren.
         meist zwischen 5 und 50
         bei kleiner perplexität: lokale variations dominate, bei großer: globale. perplexity soll nie größer sein als die anzahl der Datenpunkte, sonst wirds weird. 
     * anzahl steps: zu wenig und zu viel ist doof. 
     * öfter ausführen: evtl gibt es pro run unterschiedliche ergebnisse -.-
  Interpretation: 
    * Cluster-Size: sagt nichts aus, weil "Abstand" an regionalen Dichtevariationen im Datensatz angepasst wird und dann Abstände nich mehr aussagekräftig im globalen Vergleich.
    * Auch Distanzen zwischen den Clustern haben nicht umbedingt eine Bedeutung. 
        je nach größe des Datensatzes gibt es unterschiedliche PErplexitäts-Werte, bei denen die globale Geometrie gut zu erkennen ist. In anderen ist sie sehr verzehrt.
        -> fine-tuning perplexity um globale geometrie zu sehn.
        
tut: https://builtin.com/data-science/tsne-python 

## TODO'S VERTEILUNG: 
* API schick machen irgendwann (franzi)
* Autoencoder weiter (Paul)
* Literatur recherche, Autoencoder tuning 
    Gespräch nacharbeiten -> was is relevant für uns grade? (Priya)
 * Visualisierung (Franzi, Priya)
    
    
### Treffen BigData 26.06.23 (Franzi & Paul) 

1. Diskussion über Prerpcessing: 
        * Unterschied Standartisieren & Normalisieren, jetzt ist Standartisierung implementiert 
        * am ende validieren & vielleicht auch schauen was besser ist empirisch


STAND DER DINGE 
1. API: Franzi miuss noch weiter verfeinern, läuft soweit 
2. Preprocessing: Ist implementiert (Validierung notwendig)
3. Loom to Pandas -> schwierig mit Datengröße / HS läuft voll. 
     Paul implementiert noch Randomisiertes wählen der Stichproben. Dann ist das soweit fertig
4. Autoencoder: eine einfache Version ist implementiert
        * TODO: Architekturentscheidungen treffen & weiter verstehen 
        * TODO: verschiedene Datensätze / oder einen Datensatz mit unterschiedlichen Zelltypen druchlaufen lassen 
                    (Erwartung: Im Plot sollten da dann Gruppen raus kommen… )

5. Visualisierung: 
        * TODO: skript prepare_test_df lässt einen größeren Test durchlaufen & plottet dann nach Dimensionsreduktuion. -> Die Parameter verstehn & tunen; Ergebnisse vergleichen. 
        