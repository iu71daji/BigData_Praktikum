Your selection is: [0, 23, 28, 52, 65, 70, 76, 97]


| Nr. | cell typer and short name | file name                                      | size | 
|-----|---------------------------|------------------------------------------------|------|
| 0   | kidney                    | 0f14c412-5014-4ac0-9a71-858b2f047777.loom      | 404M |  
| 23  | eye                       | 2f8a07c2-4a6b-47ac-a304-0dead66eee9e.loom      | 610M |
| 28  | liver                     | d82664c5-9877-4c61-8591-870e59f44714.loom      | 556M |
| 52  | embryo_immune_sys         | reprogrammed-dendritic-human-embryo-10XV2.loom | 111M |
| 65  | blood_1                   | fetal-maternal-human-blood-10XV2.loom          | 337M |
| 70  | placenta                  | fetal-maternal-human-placenta-10XV2.loom       | 555M |
| 76  | decidua                   | ed360f5c-4cd1-4b01-abb2-e54da9ded89e.loom      | 257M |
| 97  | blood_2                   | systematic-comparison-human-blood-10XV2.loom   | 357M |


molecule_barcode_fraction_bases_above_30_variance (first 20):
 nan 0.0053284261375665665 nan 0.0067573231644928455 nan 0.004444883204996586 nan nan nan 0.002357723657041788 nan nan 0.004268604330718517 nan nan nan nan nan nan 0.007437548600137234
number_cells_expressing (first 20):
 9.0 581.0 1.0 371.0 nan 2442.0 nan nan nan 28.0 nan 4.0 53.0 3.0 1.0 66.0 5.0 nan nan 716.0

