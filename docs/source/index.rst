.. Autoencoder BigData_Praktikum documentation master file, created by
   sphinx-quickstart on Fri May 26 14:36:45 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Autoencoder BigData_Praktikum's documentation!
=========================================================

.. toctree::
   :caption: Contents:

   how_to_fix_loompy_error
   main_methods.py



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. note::

   This project is under active development.


Development
===========

Notes
-----

Some notes during development:

.. toctree::

   Notizen


Main documentation
==================

See API

API
===

.. autosummary::
   :nosignatures:

   main_methods.loom_to_pandas_df

