https://github.com/slundberg/shap/issues/2909

Add the following to the lines causing the error
(replacing `@jit`):

        @jit(nopython=True)