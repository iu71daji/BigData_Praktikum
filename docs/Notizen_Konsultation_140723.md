### 14.07






* manche zellen haben große unterschiede: vor alem gewebe, haut vs gehirn sollte man sehen

* krank vs. gesund hatnich so nen großen unterschied. 


### validierung: 
* nur Tsne direkt auf den input daten. 
* TSNE "parametertuning " perplexity von 20 oder so. 
* tsne schmeißt cluster gut zusammen aber erhält globale struktur schlechter als umap
* umap evtl für größere variablen / datensätze besser als tsne (?)

### Architektur 
* aktuelle forschung: latentspace doch auf 10 oder 16 runter packen. 
* also wir können unsere Architektur anpassen. 
*
Label in Loom-files nich zu finden. also nach filename gehen. 

### api: 
* venv im login node möglich, daten dann im cluster. 


prints in console werden nich geprintet
-> in logs umwandeln.   standartmäßig sind die logs in _err ( zeile 4)
gpu brauchen wir nich unmittelbar, aber code nich darauf optimiert. keras arbeitet aber eh auf gpu ? 


sbahs skirpte aus dem login home starten. 
zeile 20 cd dahin wo die skripte liegen.

venv einmal createn 
21 venv aktivieren mit source 
22 ausführung python skript.py

## abgabe 
 * minimum pipeline laufen lassen, im zweifel lokal 
* mit dokumentation
* wäre gut das aufm cluster auch zu machen 
* in vorbereitung des vortrags anpassungen möglich, hyperparameter, datensätze ändern.. 
* pypeline soll aber stehen & festgehalten sein. 
* im code nich verkünsteln. 
* testen ob repro neu geclont funktioniert, relative pfade verwenden und so. 
* Montag früh um 8 Uhr reicht im Zweifel aus.

## ToDo's 

* datensatz fertig machen: 
        * lables per file **ist fertig!**
        * endgültige Größe / Auswahl festlegen (naja wir machen unterschiedliche sets)
        * normalisierung statt standardisierung **done**
        * Skript das die richtigen Files runter lädt erstellen für server **done**
  * Autoencoder Architektur anpassen: auf 16 latentspace gehen ähnlich wie **done**
  
  * Sbash skript machen  ofenes TODO! 
  
  * API un-interaktiv machen  **done**
          -> logging für nicht aktiven part noch machen 
          -> und komische merge konflikte
          
  * TSNE auf kompletten input daten laufen lassen. **dafür gibts schon nen datensatz, nur nich gelablet** 
              -> quasi erledigt, ergebnisse schlecht. 
              → das gleiche mit dem kaggle datensatz nochmal ?
   * Latentspaces erstellen (unterschiedliche Daten)
   * Tsne auf latentspaces 
   
    ### Zur Validierung: 

    * zur Validierung: pca auf 16 runter machen und dann darauf t-sne. 
    * uuund autoencoder auf kaggel datensatz laufen lassen & darauf auch die pca - t-sne als Vergleich. 
            -> dann können wir sehn: unsere daten sin halt doof, AC vielleicht trotzdem besser als PCA, und auf guten Daten funktioniert was wir machen. 
            
    * TODO: Literatur durchsuchen: Fragen: Clustering & was geht mit test & train Daten -> machen die Leute auch die Trennung und welchen Latentspace plotten sie dann ? 
