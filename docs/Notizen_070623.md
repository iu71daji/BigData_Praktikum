# Notizen 07.06. zur Lösungsskizze bzw. dem Feedback
### Autoencoder Preprocessing
    1. loom to panda:
         wurde kritisiert, warum wir nicht was anderes verwenden. Vielleicht nochmal drüber nachdenken, alternativen suchen.. (aber eig haben die das halt vorgeschlagen und wir haben es jetz ja schon)
    2. _scheinbar brauchen wir preprocessing_
        generelle Frage: Welche Fälle können in den Daten vorkommen & wie gehen wir damit um ? 
        * also was machen wir mit NANs: rausschmeißen oder auf null setzen ? -> würde nullen die Daten verfälschen; ja schon. 
        wenn wir eh rna's rausschmeien -> vielleicht einfach alles was uns nervt, was komisch ist rausschmeißen ? 
        * Normalisierung ( hatten wir schon drin, aber im Trainingsprozess -> vorher is besser; nich im loop; sonst mehrfach; voll sinnlos.)
        * achtung, vielleicht sind die Daten ja schon normalisiert & normalisieren nach was eigentlich ? also Zeilen, Spalten, RNA-Count vermutlich.. ?
        *TODO: Rausfinden was für ne Normalisierung hier gemeint ist; sinn macht vor Eingabe in Autoencoder… ? 
        * meinen Sie z-transformation? 
        * Was sind ID's ? 
    3. FILTERUNG DER FEATURES: 
        Zitat Jan: "Typischerweise ist auch eine Filterung z.B. basierend auf der Varianz der Feature sinnvoll, um von >20.000 genes zu einem Input-Layer <10.000 zu kommen, da viele Gene irrelevant sind."
        -> seit wann das; wurde uns nich  gesagt.. 
        -> recherchieren
        -> wie umsetzen? 
        an diesem Punkt allgemein nochmal schauen wie andere das machen; Zellen mit Autoencodern encoden..  
        
### Autoencoder 
* batch size: ein batch sollte in den HS passen; wie groß ein batch wählen ? 
* keras oder pythorch ? 
    Entscheidung: erste Implementierung mit Keras machen; ist erstmal einfacher. 
* besseres Verständnis von Autoencodern generell
* besseres Verständnis unserer Daten 
### Literaturrechercher allgemein: 
* [studie](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-019-3179-5)
    hat zugänglichen code, keras aber in R, gut dokumentiert (bringt uns das was? )
 *  [studie, vergleicht unterschiedliche Autoencoder](https://www.nature.com/articles/s41587-021-01001-7)
    