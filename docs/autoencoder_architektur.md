###  Fragestellung: Welche Archtiektur ist für unsere Anwendung Sinnvoll ?

* (paper1, von Max geschickt)[https://www.frontiersin.org/articles/10.3389/fgene.2019.01205/full]
     Latentspace: 64, Input: 256, MMD was eine Methode is die zur Regularisierung eingesetzt wird. Insgesamt wurden sehr fancy Architekturen verwendet. 
 
 * (paper2, auch von Max, 2021)[https://www.mdpi.com/2072-6694/13/9/2013]
     für den einfachen Autoencoder 3 hidden layers. Insgesamt Dims ( 1000,500,100,500,1000) alsi IN-& Output 1000, ich versteh nich ganz wie es 3 innere Layer sein können ( ungerade Zahl.. ? )
     "in general vanilla and variational autoencoders showed the best performance"
     hyperbolic tangent (tanh) activation function on the input and hidden layers and sigmoid on the output layer. Loss-Function: mean-squared-error.
     vanilla == Standard-Architektur.
     
 * (paper3)[https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-019-3179-5]
     Achtung, nochmal thematisch schauen ob der Anwendungsfall auch genau unserer ist. Outcome ist für uns interessant: Hyperparameter Tuning führte zu folgender Architektur: 
      by selecting 2048 genes during random projection; producing an encoded feature space of 16 dimensions; and training the autoencoder using a learning rate of 0.001.
      Nur 5 Layer insgesamt (2048,128,16,128,2048). ReLu als Aktivierungsfunktion
      
* (paper4, 2022)[https://www.nature.com/articles/s41587-021-01001-7]
     interessant! spricht sogar vom Human Cell atlas, hier weiter schauen morgen.. :)
     hm.. check aber nich so ganz, was die da machen -,-
     irgendwelche fancy autoencoders und dann noch eine andere Methode drauf..
     

 * generell kann eine zu komplexe Architektur und ein zu großer Latentspace zu overfitting führen 
     (siehe unter anderem hier)[https://towardsdatascience.com/applied-deep-learning-part-3-autoencoders-1c083af4d798]
     
* ChatGPT schlägt vor zuerst eine einfache Archtiektur zu wählen und ggf die Anzahl der Layer usw. zu erweitern. 
    auch zum Latent Space sagt Chatti: erst einen kleineren wählen, bei Bedarf dann vergrößern. 
    
 ## Sitenotes: 
 * Paper 2 kommt zum Schluss, dass  K-Medoid Clustering (PAM)Algorithm mit Spearman similarity zu besseren Ergebnissen führt als k-means/Euclidean clustering. 
     -> also vielleicht das verwenden. Auch Paper3 macht Aussagen zum Clustering
 * nur Paper 1 + 2 von Max; Rest aus der Internet-Wildnis.

### Einschätzung soweit: 
* wir können easy argumentieren, dass wir einen simplen vanilla autoencoder gewählt haben. (siehe Paper2, da beste performance) und allgemein die Argumentation "im Zweifel simpler". 
* Dann nich zu komplex, wegen Gefahr von Overfitting. 
* welche Layers, wie viele… ? Wir müssen uns die Input-Size unserer Daten nochmal anschauen und ich tendiere dazu eine Archtiektur zwischen den Vorschlägen von Paper2 und Paper3 zu wählen. 
    Ceck die Inputdaten grad nich in ihren Dims, @Pauli :) 