import requests
import os
import sys
from tqdm import tqdm
from tabulate import tabulate
from api_variables import *
import logging

####################################################################
##    helper-functions needed for at least one of the 2 useceses  ##
####################################################################


def download_file_fromIndex(index, save_location, files_list):
    """
    actually downloading, code used from here:
    https://github.com/DataBiosphere/azul/blob/develop/docs/download-project-matrices.ipynb

    :param index:
    :param save_location:
    :param files_list:
    :return:
    """
    for i in index:
        url_tp = files_list[i]['url']
        # Work around https://github.com/DataBiosphere/azul/issues/2908
        url = url_tp.replace('/fetch', '')

        # modify filename (add celltype, necesssary for labeling)
        file_name = str(files_list[i]['organ']) + "__" + str(files_list[i]['fileName'])
        filename_clean = file_name.replace("[", "").replace("]", "").replace("'", "")
        output_path = os.sep.join([save_location, filename_clean])

        # test if file already exists to avoid multiple downloads
        if os.path.exists(output_path):
            print(f"file {files_list[i]['fileName']} already exists in save directory and won't be downloaded.")
        else:
            response = requests.get(url, stream=True)
            response.raise_for_status()

            total = int(response.headers.get('content-length', 0))
            print(f'Downloading to: {output_path}', flush=True)

            with open(output_path, 'wb') as f:
                with tqdm(total=total, unit='B', unit_scale=True, unit_divisor=1024) as bar:
                    for chunk in response.iter_content(chunk_size=1024):
                        size = f.write(chunk)
                        bar.update(size)


def download_all_files(response, save_location, usecase2):
    """
    Function gets metadata of loom-files and downloads the given files into the specified save location.
    actually downloading, code used from here:
    https://github.com/DataBiosphere/azul/blob/develop/docs/download-project-matrices.ipynb
    :param response:
    :param save_location:
    :return:
    """
    # save metadata in list
    file_metadata = []
    for hit in response['hits']:
        for file in hit['files']:

            file_urls = dict(fileName=file['name'], url=file['url'],
                             organ = hit['cellSuspensions'][0]['organ'])
            file_metadata.append(file_urls)
    # download files
    for file in file_metadata:
        url_tp = file['url']
        url = url_tp.replace('/fetch', '')              # Work around https://github.com/DataBiosphere/azul/issues/2908
        # modify filename (add celltype, necesssary for labeling)
        file_name = str(file['organ']) + "__" + str(file['fileName'])
        filename_clean = file_name.replace("[", "").replace("]", "").replace("'", "")
        output_path = os.sep.join([save_location, filename_clean])

        # test if file already exists to avoid multiple downloads
        if os.path.exists(output_path):
            print(f"file {file['fileName']} already exists in save directory and won't be downloaded.")
            if usecase2: logging.info(f"file {file['fileName']} already exists in save directory and won't be downloaded.")
        else:
            response = requests.get(url, stream=True)
            response.raise_for_status()
            total = int(response.headers.get('content-length', 0))
            print(f'Downloading to: {output_path}', flush=True)
            if usecase2: logging.info('Downloading to: %s', output_path)

            with open(output_path, 'wb') as f:
                with tqdm(total=total, unit='B', unit_scale=True, unit_divisor=1024) as bar:
                    for chunk in response.iter_content(chunk_size=1024):
                        size = f.write(chunk)
                        bar.update(size)


def get_files_metadata(usecase2,
                       numberFiles=100,
                       sort='lastModifiedDate',
                       order='asc',
                       projectId=False,
                       projectTitle=False,
                       organ=False,
                       donor_species=False,
                       sampleDisease=False,
                       cellType=False):
    """
    Function does validation-checks on the filter-variables. Then it takes the given variables and puts them together in
    a string-format, which is needed for the API request. It requests the file metadata, performs a sanity check on the
    response and returns the response as dictionary.
    :param numberFiles: specifies the maximum number of files whose metadata should be displayed
    :param sort: the value by which the files are to be sorted
    :param order: 'asc' or 'desc'
    :param projectId: single projectId as string or a list of projectIds
    :param projectTitle: single projectTitle as string or a list of projectTitles
    :param organ: single organ from which the cells originate as string or a list of organs
    :param donor_species: species from which the cells originate, one as string or multiple in a list ( e.g. Homo sapiens)
    :param sampleDisease: disieases which the cell donor has, one as string or multiple in a list
    :param cellType: filter by cell types, one type as string or multiple in a list
    :return: api response as dictionary
    """
    params = {
        'catalog': 'dcp29',
        'size': numberFiles,
        'sort': sort,
        'order': order,
    }
    # check input variables
    if not (isinstance(numberFiles, int) and numberFiles > 0):
        print("Number of Files has to be an integer greater zero.")
        if usecase2: logging.error("Number of Files has to be an integer greater zero.")
        return
    if not is_valid_input("order", order,usecase2):
        filter_error("order", usecase2)
    if not is_valid_input("sort", sort, usecase2):
        filter_error("sort", usecase2)

    # create filter-string according to the given variables and check for valide inputs
    filters_string = '{"fileFormat": {"is": ["loom","loom.gz"]}'
    if projectTitle:
        if isinstance(projectTitle, list): projectTitle = '", "'.join(projectTitle)
        filters_string +=  f''', "projectTitle": {{"is": ["{projectTitle}"]}}'''
    if projectId:
        if isinstance(projectId, list): projectId = '", "'.join(projectId)
        filters_string += f''', "projectId": {{"is": ["{projectId}"]}}'''
    if organ:
        if not is_valid_input("organ", organ, usecase2):
            filter_error("organ", usecase2)
        else:
            if isinstance(organ, list): organ = '", "'.join(organ)
            filters_string += f''', "organ": {{"is": ["{organ}"]}}'''
    if donor_species:
        if not is_valid_input("donor_species", donor_species, usecase2):
            filter_error("donor_species", usecase2)
        else:
            if isinstance(donor_species, list): donor_species = '", "'.join(donor_species)
            filters_string += f''', "genusSpecies": {{"is": ["{donor_species}"]}}'''
    if sampleDisease:
        if not is_valid_input("sampleDisease", sampleDisease, usecase2):
            filter_error("sampleDisease", usecase2)
        else:
            if isinstance(sampleDisease, list): sampleDisease = '", "'.join(sampleDisease)
            filters_string += f''', "sampleDisease": {{"is": ["{sampleDisease}"]}}'''
    if cellType:
        if not is_valid_input("cellType", cellType, usecase2):
            filter_error("cellType", usecase2)
        else:
            if isinstance(cellType, list): cellType = '", "'.join(cellType)
            filters_string += f''', "selectedCellType": {{"is": ["{cellType}"]}}'''

    filters_string += "}"
    print("filters string : ", filters_string)
    params['filters'] = filters_string

    # request metadate from API
    url = f'https://service.azul.data.humancellatlas.org/index/files'
    response = requests.get(url, params=params)

    check_response(response, usecase2)
    response_dict = response.json()
    return response_dict


def get_file_list(response_dict):
    # extract file-information into a list of dicts
    file_data = []  # list
    for hit in response_dict['hits']:
        for file in hit['files']:
            file_dict = dict(fileName=file['name'],
                             size=round((file['size'] / 1000000), 2),
                             version=file['version'],
                             projectShortname=hit['projects'][0]['projectShortname'][0],
                             projectId=hit['projects'][0]['projectId'][0],
                             projectTitle=hit['projects'][0]['projectTitle'][0], entryId=hit['entryId'],
                             contentDescription=file['contentDescription'][0],
                             donorOrganism=hit['donorOrganisms'][0]['genusSpecies'][0],
                             donorDisease=hit['donorOrganisms'][0]['disease'],
                             organ=hit['cellSuspensions'][0]['organ'],
                             totalCells= hit['cellSuspensions'][0]['totalCells'],
                             cellType =hit['cellSuspensions'][0]['selectedCellType'],
                             url=file['url'])
            file_data.append(file_dict)
    file_data_with_index = [{
        'index': i,
        **file_dict
    } for i, file_dict in enumerate(file_data)]
    return file_data_with_index


def print_file_table(file_data):
    # create nested list out of file_data
    headers = ["Index", "File Name", "Size in MB", "Version", "ProjectTitle", "Organ", "Cells in total",
               "Donor", "Donor Disease", "Cell Type"]
    table_data = [[
        file['index'],
        file['fileName'],
        file['size'],
        file['version'],
        file['projectTitle'],
        file['organ'],
        file['totalCells'],
        file['donorOrganism'],
        file['donorDisease'],
        file['cellType']
    ] for file in file_data]

    table = tabulate(table_data, headers, tablefmt='fancy_grid')
    print(table)

def check_quotation_mark(var, input):
    if input == "?":
        print("valid inputs :")
        print(print_validation_list(get_validation_list(var, usecase2=False)))
        print("try again: ")
        return True
    else: return False



def define_filter_variables_console():
    print(welcomeText)
    print(filtervariablesText)
    projectId = input("Project ID: ").strip()
    projectTitle = input("Project Title: ").strip()

    repeat = True
    while repeat:
        organ = input("Organ (e.g. heart) : ").strip()
        repeat = check_quotation_mark("organ", organ)

    repeat = True
    while repeat:
        donor_species = input("Donor species (e.g. Homo sapiens): ").strip()
        repeat = check_quotation_mark("donor_species", donor_species)

    repeat = True
    while repeat:
        sampleDisease = input("Sample Disease (e.g. COVID-19): ").strip()
        repeat = check_quotation_mark("sampleDisease", sampleDisease)

    repeat = True
    while repeat:
        cellType = input("Cell Type (e.g. plasma cell): ").strip()
        repeat = check_quotation_mark("cellType", cellType)

    repeat = True
    print(hits_order_Text)
    numberFiles = input("How many hits should be loaded max. (default: first 100 hits): ")
    order = input("Order files 'asc' or 'desc' (default is 'asc'): ")

    while repeat:
        print("for the following parameter you can type '?' again to see the options: ")
        sort = input("Sort files by ? (default: 'lastModifiedDate'): ")
        repeat = check_quotation_mark("sort", sort)

    # check if default values where adjusted, if not give defaults to the variables:
    if "" == numberFiles.strip():
        numberFiles = int(100)
    if sort.strip() == "":
        sort = 'lastModifiedDate'
    if order.strip() == "":
        order = 'asc'


    # set not specified filter-variables to false if empty, to list if multiple parameters, to string if one:
    if projectId .strip() == "": projectId_final = False
    elif len(projectId.split(", ")) > 1: projectId_final = projectId.split(", ")
    else: projectId_final = projectId

    if projectTitle .strip() == "": projectTitle_final = False
    elif len(projectTitle.split(", ")) > 1: projectTitle_final = projectTitle.split(", ")
    else: projectTitle_final = projectTitle

    if organ.strip() == "": organ_final = False
    elif len(organ.split(", ")) > 1: organ_final = organ.split(", ")
    else: organ_final = organ

    if donor_species.strip() == "": donor_species_final = False
    elif len(donor_species.split(", ")) > 1: donor_species_final = donor_species.split(", ")
    else: donor_species_final = donor_species

    if sampleDisease.strip() == "": sampleDisease_final = False
    elif len(sampleDisease.split(", ")) > 1: sampleDisease_final = sampleDisease.split(", ")
    else: sampleDisease_final = sampleDisease

    if cellType.strip() == "": cellType_final = False
    elif len(cellType.split(", ")) > 1: cellType_final = cellType.split(", ")
    else: cellType_final = cellType

    dictionary = dict(numberFiles = int(numberFiles),
                sort = sort,
                order= order,
                projectId = projectId_final,
                projectTitle = projectTitle_final,
                organ = organ_final,
                donor_species = donor_species_final,
                sampleDisease = sampleDisease_final,
                cellType = cellType_final
    )
    return dictionary


##################################################################
##      functions helping to validate input and API response    ##
##################################################################
def get_validation_list(variable, usecase2):
    """
    function takes lists with valid inputs for the variables donor_species, organ, sampleDisease, cellType
    and sort and returns them on demand. The lists are stored in api_variables.py for better readability.
    :param variable:
    :return:
    """
    if variable == "donor_species":
        return donor_species_list
    elif variable == "sort":
        return sort_list
    elif variable == "organ":
        return organ_list
    elif variable == "order":
        return order_list
    elif variable == "sampleDisease":
        return sampleDisease_list
    elif variable == "cellType":
        return cellType_list
    else:
        print("something went wrong, given variable has no list stored")
        if usecase2: logging.error("something went wrong, given variable has no list stored")

def print_validation_list(list):
    for i in range(0, len(list), 10):
        row_values = list[i:i + 10]
        if len(row_values) == 10:
            row_string = ', '.join(str(value) for value in row_values)
            print(row_string)

    if len(list) % 10 != 0:
        row_values = list[-(len(list) % 10):]
        row_string = ', '.join(str(value) for value in row_values)
        print(row_string)


def is_valid_input(variable, input, usecase2):
    """
    function checks if input is valid by checking if it is in the list of valid valuse for the variables sort,
    organ and donor_species, depending on the given variable.
    :param variable: sort, organ or donor_species
    :return: True if valid, false if not.
    """
    list1 = get_validation_list(variable, usecase2)
    # für listen
    if isinstance(input, list) and not all(elem in list1 for elem in input) \
            or isinstance(input,str) and input not in list1:
        return False
    else:
        return True

def filter_error(variable, usecase2):
    """
    function gets a filter variable and the list of valid strings for this variable and
    prints out an error message.
    """
    list = get_validation_list(variable, usecase2)
    print(f"Error, given {variable} does not exist")
    print("check for spelling mistake!")
    print(f"valid {variable}s are :")
    print_validation_list(list)
    if usecase2:
        logging.error(f"Error, given {variable} does not exist")
        logging.debug("check for spelling mistake!")
        logging.info(f"valid {variable}s are :", list)
    raise ValueError




def check_response(response, usecase2):
    # check status code
    if not response.status_code == 200:
        if usecase2:
            logging.error("Error: no connection to API, status code is: ", response.status_code)
        raise RuntimeError("Error: no connection to API, status code is: ", response.status_code)
    # check if response is None-type
    if response is None:
        if usecase2:
            logging.error("Error: response is None-Type, check if filter-variables were set correct.")
        raise TypeError("Error: response is None-Type, check if filter-variables were set correct.")
    # check if request check resulted in hits
    response_dict = response.json()
    if len(response_dict['hits']) == 0:
        if usecase2:
            logging.warning("No Files found for given search criterias.")
        raise ValueError("No Files found for given search criterias.")


##################################################################
##     the two main functions, managing the hole task           ##
##################################################################

def get_savelocation():
    # Get the location to download data to
    print(f"files will be downloaded to '{os.path.join(os.getcwd(),'data')}' by default")
    again = True
    while again:
        again = False
        path = input("To specify an alternative path, type it in here: ")
        if path.strip() != '':
            again = not(os.path.exists(path))
            if again: print("Sadly this path does not exist,try again! ")
        else:
            path = os.getcwd()
    save_location = os.path.join(path, 'data')
    print("save_location: ", save_location)
    #check if folder data exists in save_location and create ./data if not
    if not os.path.exists(save_location):
        os.makedirs(save_location)

    return save_location

def usecase2(dict, save_location):
    """
    not interactive, gets the meta-data and downloads the files
    :return:
    """
    logging.basicConfig(filename='logs.log', level=logging.DEBUG)

    response_dict = get_files_metadata(numberFiles=dict['numberFiles'],
                                       sort=dict['sort'],
                                       order=dict['order'],
                                       projectId=dict['projectId'],
                                       projectTitle=dict['projectTitle'],
                                       organ=dict['organ'],
                                       donor_species=dict['donor_species'],
                                       sampleDisease=dict['sampleDisease'],
                                       cellType=dict['cellType'],
                                       usecase2 = True)

    # download the files
    download_all_files(response_dict, save_location, usecase2=True)

def usecase1_interactive():
    # get filter-variables through console:
    again = True
    while again == True:
        again = False
        dict = define_filter_variables_console()
        print(dict)
        # get file-metadata & check if dict_values are valid:
        try:
            response_dict = get_files_metadata( usecase2=False,
                                    numberFiles=dict['numberFiles'],
                                      sort= dict['sort'],
                                      order= dict['order'],
                                      projectId= dict['projectId'],
                                      projectTitle= dict['projectTitle'],
                                      organ= dict['organ'],
                                      donor_species= dict['donor_species'],
                                      sampleDisease= dict['sampleDisease'],
                                      cellType= dict['cellType'])
        except ValueError:
            print("No Files found for given search criterias.")
            print("check for spelling mistakes! ")
            var = input("type Y if you want to restart, press enter to end : ")
            if "" == var.strip():
                print("Exiting the script.")
                sys.exit()
            if "Y" == var.strip():
                again = True

    files_list = get_file_list(response_dict)


    # get indexes to download via console:
    print_file_table(files_list)
    print("Type the indices you want to download separated by a space.")
    inputs = input("Example: 1 17, your input: ")
    index = list()
    for i in inputs.split(" "):
        try:
            i = int(i)
        except:
            print("Input is empty or invalid")
            print("Please only use numbers as indices!")
            continue
        index.append(i)
    print("Your selection is:", index)

    # Get the right location to download data to
    save_location = get_savelocation()
    # download data
    if len(index) != 0:
        download_file_fromIndex(index, save_location, files_list)


def download_looms_by_organ_list(organ_lst=None, nr_files: int = 1,
                                 species: str = "Homo sapiens"):
    """
    Downloads nr_files many .loom file for each organ in organ_lst.
    :param species: the donor species of the cell data.
    :param nr_files: number of files to be downloaded per organ.
    :param organ_lst: list of organ strings.
    :return: None
    """
    if organ_lst is None:
        organ_lst = ['heart', 'blood', 'lung', 'brain',
                     'kidney', 'eye', 'liver']

    for organ in organ_lst:

        filter_variables = dict(numberFiles=nr_files,
                                sort='lastModifiedDate',
                                order='asc',
                                projectId=False,
                                projectTitle=False,
                                organ=organ,
                                donor_species=species,
                                sampleDisease=False,
                                cellType=False)

        # save_location (default ./data )
        cwd = os.getcwd()
        save_location = os.path.join(cwd, 'data')

        # check if folder data exists in save_location and create ./data if not
        if not os.path.exists(save_location):
            os.makedirs(save_location)

        # download data:
        usecase2(filter_variables, save_location)
