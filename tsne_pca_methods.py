import os
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from data_cleaning import center_data


def plot_six_plots(results, labels, param_list, param_name, title=False,
                   pdf_only=False, filename=None):
    # plot_data(results)
    fig, axis = plt.subplots(2, 3)
    if title:
        fig.suptitle(title)
    for t in range(len(param_list)):
        x_ax = t // 3
        y_ax = t % 3
        if t == 0:
            tmp = sns.scatterplot(x='x', y='y', data=results[t], hue=labels)
            handles, labels_temp = tmp.get_legend_handles_labels()
            fig.legend(handles, labels_temp, loc='lower center', ncol=len(labels_temp))
        sns.scatterplot(x='x', y='y', data=results[t], hue=labels, ax=axis[x_ax, y_ax], legend=False)
        axis[x_ax, y_ax].set_title(f'{param_name} {results[t][param_name][0]}')

    plt.tight_layout()
    if pdf_only and filename is not None:
        plt.savefig(filename)
        del fig
    else:
        plt.show()


def plot_data(results, labels, title=False, pdf_only=False, filename=None):
    # convert np-array to pd-dataframe
    # sns.lmplot(x ="first_dim", y="second_dim", data=df, fit_reg=False)
    fig = sns.scatterplot(x='x', y='y', data=results[0], hue=labels)
    if title:
        plt.title(title)
    if pdf_only and filename is not None:
        plt.savefig(filename)
        del fig
    else:
        plt.show()


def pca_on_ac_input(x_train, x_test):
    # center data, necessary for PCA:
    x_train = center_data(x_train)
    x_test = center_data(x_test)
    # Convert the DataFrames
    x_train = x_train.values
    x_test = x_test.values

    # Perform PCA
    pca = PCA(n_components=16)
    pca.fit(x_train)

    x_test_pca = pca.transform(x_test)

    # Calculate explained variance by first 16 components
    explained_variance_ratio = pca.explained_variance_ratio_
    cumulative_variance_ratio = np.cumsum(explained_variance_ratio)
    variance_explained = cumulative_variance_ratio[15] * 100
    print(f"Variance explained by the first 16 principal components: {variance_explained:.2f}%")

    return x_test_pca


def tsne_perplexities(perplexities, df):
    """
    function for doing tsne on different perplexities, made for 6 plots,
    but adjustable for less.
    """

    # print("number of different perplexities : ", len(perplexities))
    results = []
    for p in perplexities:
        # model
        m = TSNE(perplexity=p, n_iter=5000)

        # t-SNE features are computed
        tnse_features = m.fit_transform(df)
        perc = [p] * tnse_features.shape[0]
        d_dict = {'perplexity': perc,
                  'x': tnse_features[:, 0],
                  'y': tnse_features[:, 1]}
        results.append(d_dict)
    return results

###########################################################################
# T-sne mit 5 unterschiedlichen Lernraten und perplexity = 30      ##
###########################################################################


def tsne_learningrates(learningrate, df):
    results = []
    for lr in learningrate:
        # model
        m = TSNE(perplexity=40, learning_rate=lr, n_iter=5000)

        # t-SNE features are computed and a subset is printed
        tnse_features = m.fit_transform(df)
        learn = [lr] * 8
        d_dict = {'learningrate': learn, 'x': tnse_features[:, 0], 'y': tnse_features[:, 1]}
        results.append(d_dict)
    return results
