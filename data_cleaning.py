from pandas import DataFrame
import numpy as np


#########################################
# Data Cleaning / Preprocessing methods #
#########################################

def center_data(df: DataFrame, axis=0):
    """
    Returns the same DataFrame but with centered data.
    :param df: DataFrame to be centered
    :param axis: axis to be used
    :return: pandas DataFrame
    """
    # centering the data
    return df - df.mean(axis=axis)


def normalize_data(df: DataFrame, axis=0):
    """
    Returns the same DataFrame but with normalized rows.
    :param df: DataFrame to be normalized
    :param axis: axis to be used
    :return: pandas DataFrame
    """
    # Normalize objects/rows (axis=0) using min-max feature scaling
    return (df - np.min(df, axis)) / (np.max(df, axis) - np.min(df, axis))


def standardize_data(df: DataFrame, axis=0):
    """
    Returns the same DataFrame standardized using Z-Score
    :param axis: axis to be used
    :param df: DataFrame to be standardized
    :return: pandas DataFrame
    """
    # Standardize the data
    return (df - np.mean(df, axis=axis)) / np.std(df, axis=axis)
    # scaler = StandardScaler()
    # return pd.DataFrame(scaler.fit_transform(df))


def drop_insignificant_data(df: DataFrame, threshold=0,
                            nr_cols=None, ac_first_layer=128):
    """
    Returns the same DataFrame without columns (genes) which have variance
    smaller than some threshold.
    :param df: DataFrame for which zero-variance columns are to be removed
    :param nr_cols: select this many attributes which have the highest variance
    :param ac_first_layer: nr. of features the autoencoder needs at least
    :param threshold: the minimum variance of an attribute to be kept in df
    :return: pandas DataFrame
    """
    # Calculate the variances
    variances = df.var(axis=0)

    # Create boolean sequence, True if the gene is to be chosen
    if nr_cols is not None:
        # variances = variances.sort_values(ascending=False)
        # Remove zero-values
        non_zero = variances[variances != 0]
        l_nz = len(non_zero)
        if l_nz > nr_cols:  # Reduce features further (RNAs with high variance)
            non_zero = non_zero.sort_values(ascending=False)
            indexes = non_zero[:nr_cols].index
        else:
            print("WARNING")
            if l_nz < ac_first_layer:  # Nr. of features the ac needs at least
                print(f"Data contains only {l_nz} features with variance > 0")
                print(f"but still contain {nr_cols - l_nz} var=0 features!")
                variances = variances.sort_values(ascending=False)
                indexes = variances[:nr_cols].index
            else:  # Nr. of features is between ac_first_layer and nr_cols (OK)
                indexes = non_zero.index
                print(f"The DataFrame was reduced to {l_nz} features!")
    else:
        indexes = variances[variances > threshold].index
    # Pick the selected columns (Genes) and return the reduced df
    df = df.loc[:, indexes]
    return df


def preprocessing(df: DataFrame, scaling="none",
                  threshold=0, nr_cols: int = None):
    """
    The preprocessing includes:
    - dropping of columns that contain None/NaN values
    - normalization of object values (rows)
    - dropping columns with variance less than min_variance value
    :param df: pandas DataFrame to be pre-processed
    :param scaling: options are "standardize", "normalize", "center" (default)
    :param threshold: the minimum variance of an attribute to be kept in df
    :param nr_cols: select this many attributes which have the highest variance
    :return: preprocessed pandas DataFrame
    """
    # Drop NaN values
    cols_before = df.shape[1]
    df = df.dropna(how='any', axis=1)
    cols_no_nan = df.shape[1]
    print(f"Drop {cols_before - cols_no_nan} features, because of NaN values.")

    # Drop insignificant (columns/attributes with little variance)
    if nr_cols is None:
        df = drop_insignificant_data(df, threshold=threshold)
    else:
        df = drop_insignificant_data(df, nr_cols=nr_cols)
    print(f"Drop {cols_no_nan - df.shape[1]} features with 0 or low variance.")

    # Feature scaling
    print(f"Feature scaling: {scaling} data..")
    if scaling == "standardize":
        df = standardize_data(df)
    elif scaling == "normalize":
        df = normalize_data(df)
    elif scaling == "center":
        df = center_data(df)
    else:
        print("No scaling.")

    return df
