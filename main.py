from data_sampling import *
from data_cleaning import *
from sys import getsizeof
from data_cleaning import preprocessing
from sklearn.model_selection import train_test_split
from autoencoder_methods import *
from tsne_pca_methods import *

#######################
# SETTINGS            #
#######################

# looms = None  # None => all
looms = ["eye__8b81f0c2-3b57-4072-8a10-c169fece543e.loom",
         "lung__098cc66a-d806-42db-a1c8-fa99a0317d7c.loom"]

file_name_ending = "main"
nr_cols = 20000  # None => all
nr_rows = 900  # None => all  (rows per file)
# For splitting
test_size = 0.3
random_state = 42  # Seed for the split
# Preprocessing
scaling = "normalize"  # "normalize", "none", "center", "standardize"
clean_down_to = 2048  # Nr features to keep after cleaning
# autoencoder
epochs = 30
batch_size = 150
ac_input_dim = 2048
ac_layer_dims = (128, 16, 128)
# T-SNE
perplexity = 20
perplexities = [10, 20, 40, 60, 80, 100]


#############
# LOCATIONS #
#############
cwd = os.getcwd()
data_path = check_make_path('data')
files_path = check_make_path("files")
results_path = check_make_path('results')


###############################################
# LOOM FILES                                  #
###############################################
if looms is None:
    looms = [x for x in get_list_of_loom_files() if "__" in x]
print("The following loom files will be used to run the autoencoder on:",
      *looms, sep="\n")

###############################################
# CREATE DATAFRAME and save as PICKLE         #
###############################################

print(f"Create DataFrame with {nr_cols or 'all'} features",
      f"and {nr_rows or 'all'} rows per file")

data, o_lbls, a_lbls = sample_of_several_looms(looms, nr_cols, nr_rows)
df = DataFrame(data, index=o_lbls, columns=a_lbls)
print(f"DataFrame df has size: {getsizeof(df) / 1000000.} MB")

print("DF:\n", df)

###################################
# PREPROCESSING & SCALING         #
###################################

# Cleaning & scaling
df = preprocessing(df, scaling=scaling, nr_cols=clean_down_to)
print("DF after preprocessing:\n", df)

# Save the DataFrame
save_df(df, name=f"clean_test_df_{file_name_ending}", directory=files_path)

#######################################
# Split into training and test data   #
#######################################

x_train, x_test = train_test_split(df, test_size=test_size,
                                   random_state=random_state)
print("Training set:\n", x_train)
# save_df(df, name=f"x_train_{file_name_ending}", directory=files_path)

print("Test set:\n", x_test)
# save_df(df, name=f"x_test_{file_name_ending}", directory=files_path)

#############################################
#       APPLY AUTOENCODER ON df             #
#############################################

# Define model
autoencoder, encoder, decoder = define_autoencoder(x_train.shape[1],
                                                   ac_layer_dims)
# Compile the autoencoder model
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')

# Train the model
history = autoencoder.fit(x_train, x_train,
                          epochs=epochs,
                          batch_size=batch_size,
                          shuffle=True,
                          validation_data=(x_test, x_test))

# Save models
# autoencoder.save(f"autoencoder_model_{file_name_ending}.keras")
# decoder.save(f"decoder_model_{file_name_ending}.keras")

# Plot
fig_loss_hist = plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
results_file = os.path.join(results_path, f"00_loss_history_{file_name_ending}.pdf")
plt.savefig(results_file)
plt.show()

# Apply model
latent_space = DataFrame(encoder.predict(x_test))
# Save the latent_space as pandas DataFrame
save_df(latent_space, name=f"latent_space_{file_name_ending}", directory=files_path)
print(f"Latent_space df has size: {getsizeof(latent_space) / 1000000.} MB")
print(latent_space)

############################################
#        TSNE ON LATENTSPACE               #
############################################

# get labels for x_test:
test_labels = get_data_labels(x_test)

# Tsne on latent-space:
single_res_tsne_ac = tsne_perplexities(perplexities=[perplexity], df=latent_space)

# Tsne for different perplexities on latent-space
multi_res_tsne_ac = tsne_perplexities(perplexities=perplexities, df=latent_space)

# plot results
title1 = "Result T-sne on latent space, with perplexity 20  (HumanCellAtlas)"
print("len lables: ", len(test_labels))
plot_data(results=single_res_tsne_ac, labels=test_labels, title=title1,
          pdf_only=True,
          filename=os.path.join(results_path, "01_T-sne_LatSpace_ac_p20.pdf"))

title2 = "tsne on pca results, different perplexities (HumanCellAtlas)"
plot_six_plots(results=multi_res_tsne_ac,
               labels=test_labels,
               param_list=perplexities,
               param_name="perplexity",
               title=title2,
               pdf_only=True,
               filename=os.path.join(results_path, "02_multi_res_tsne_AC_6_plots.pdf"))


######################################################################################
#     PCA on Autoencoder Input data and t-sne on first 16 principal components       #
######################################################################################

transformed_test_data = pca_on_ac_input(x_train=x_train, x_test=x_test)


##########################################
#    t-sne on principal components       #
##########################################

# Tsne for perplexity = 30
sing_res_tsne_pca = tsne_perplexities(perplexities=[30], df=transformed_test_data)

# Tsne for different perplexities
multi_res_tsne_pca = tsne_perplexities(perplexities=perplexities, df=transformed_test_data)

# plot results
title1 = "Result T-sne after PCA, with perplexity 30  (HumanCellAtlas)"
plot_data(sing_res_tsne_pca, test_labels, title1,
          pdf_only=True,
          filename=os.path.join(results_path, "03_T-sne_after_PCA_p30.pdf"))

title2 = "tsne on pca results, different perplexities (HumanCellAtlas)"
plot_six_plots(results=multi_res_tsne_pca,
               labels=test_labels,
               param_list=perplexities,
               param_name="perplexity",
               title=title2,
               pdf_only=True,
               filename=os.path.join(results_path, "04_multi_res_T-sne_PCA_6_plots.pdf"))

print("FIN\n\n", "See results (plots) in results folder")
