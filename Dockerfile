FROM python:3.9.17

# Set working directory
WORKDIR /app

# Copy all files
COPY autoencoder_methods.py data_sampling.py main.py api_access_main.py \
        main_download_looms.py tsne_pca_methods.py api_methods.py \
        main_on_kaggle.py api_variables.py data_cleaning.py requirements.txt \
        ./

# install requirements
RUN pip install --no-cache-dir -r requirements.txt

# Run script file
CMD ["python", "main_download_looms.py", "&", "python", "main.py"]


