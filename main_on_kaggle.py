from data_sampling import *
from data_cleaning import *
from sklearn.model_selection import train_test_split
from autoencoder_methods import *
from tsne_pca_methods import *

#######################
# SETTINGS            #
#######################
# For splitting
test_size = 0.3
random_state = 42  # Seed for the split
# Preprocessing
scaling = "normalize"  # "normalize", "none", "center", "standardize"
clean_down_to = 2048  # Nr features to keep after cleaning
# autoencoder
epochs = 30
batch_size = 150
ac_input_dim = 2048
ac_layer_dims = (128, 16, 128)
# T-SNE
perplexities = [10, 20, 40, 60, 80, 100]


#############
# LOCATIONS #
#############
cwd = os.getcwd()
data_path = check_make_path('data')
files_path = check_make_path("files")
results_path = check_make_path('results')
kaggle_path = check_make_path('kaggle_data')


##################################################
#     other single cell data set from kaggle     #
##################################################

#################################
# load data & preprocessing     #
#################################

kaggle_df = load_df("clean_kaggle_df", directory=kaggle_path)
print(kaggle_df)

# Get train and test set
x_train_kaggle, x_test_kaggle = train_test_split(kaggle_df, test_size=test_size,
                                                 random_state=random_state)

# drop labels and save them in separate lists:
# train_labels_kaggle = x_train_kaggle['label']
test_labels_kaggle = x_test_kaggle['label']
x_train_kaggle = x_train_kaggle.drop('label', axis=1)
x_test_kaggle = x_test_kaggle.drop('label', axis=1)

##########################################
#       autoencoder on kaggle data       #
##########################################
# (same architecture, we reuse the specified parameters from the first model)

# Define model
ac_kaggle, encoder_kaggle, _ = define_autoencoder(x_train_kaggle.shape[1],
                                                  ac_layer_dims)

# Compile the autoencoder model
ac_kaggle.compile(optimizer='adam', loss='binary_crossentropy')

# Train the model
history_kaggle = ac_kaggle.fit(x_train_kaggle, x_train_kaggle,
                               epochs=epochs,
                               batch_size=batch_size,
                               shuffle=True,
                               validation_data=(x_test_kaggle, x_test_kaggle))

# Plot
plt.plot(history_kaggle.history['loss'])
plt.plot(history_kaggle.history['val_loss'])
plt.title('model loss autoencoder trained on kaggle data')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# Apply model
latent_space_kaggle = DataFrame(encoder_kaggle.predict(x_test_kaggle))
# Save the latent_space as pandas DataFrame
save_df(latent_space_kaggle, name=f"latent_space_kaggle", directory=files_path)

print("latentspace kaggle: ")
print(latent_space_kaggle)


#################################
#      TSNE ON LATENTSPACE      #
#################################

# Tsne for perplexity = 20
single_tsne_kaggle = tsne_perplexities(perplexities=[20],
                                       df=latent_space_kaggle)

# plot results
title1 = "Result T-sne on latent space, with perplexity 20  (kaggle data)"
plot_data(results=single_tsne_kaggle, labels=test_labels_kaggle, title=title1)

# Tsne for different perplexities
multi_tsne_kaggle = tsne_perplexities(perplexities=perplexities,
                                      df=latent_space_kaggle)

title2 = "T-sne after pca - different perplexities (kaggle data)"
plot_six_plots(results=multi_tsne_kaggle,
               labels=test_labels_kaggle,
               param_list=perplexities,
               param_name="perplexity",
               title=title2)


#########################################################
#       PCA on kaggle data instead og autoencoder       #
#########################################################

transformed_kaggle_test_data = pca_on_ac_input(x_train=x_train_kaggle,
                                               x_test=x_test_kaggle)

##############################################
#   TSNE ON FIRST 16 PRINCIPAL COMPONENTS    #
##############################################


# Tsne for perplexity = 30
sing_res_tsne_pca_kaggle = tsne_perplexities(perplexities=[30],
                                             df=transformed_kaggle_test_data)


# Tsne for different perplexities
multi_res_tsne_pca_kaggle = tsne_perplexities(perplexities=perplexities,
                                              df=transformed_kaggle_test_data)

# plot results
title1 = "Result T-sne after PCA, with perplexity 30  (kaggle data)"
plot_data(sing_res_tsne_pca_kaggle, test_labels_kaggle, title1)


title2 = "Result T-sne after PCA, different perplexities (kaggle data)"
plot_six_plots(results=multi_res_tsne_pca_kaggle,
               labels=test_labels_kaggle,
               param_list=perplexities,
               param_name="perplexity",
               title=title2)
