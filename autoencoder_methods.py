from keras.layers import Input, Dense, LeakyReLU
from keras.models import Model


def define_autoencoder(input_dim: int = 2048,
                       layer_dims: tuple = (128, 16, 128)):
    """
    :param input_dim: nr. features of the input data
    :param layer_dims: tuple of ints for the layer architecture
    :return: keras.models.Model (autoencoder Model)
    """
    # CHECK
    if len(layer_dims) != 3 or not all(
            [type(1) == type(x) for x in layer_dims]):
        print("Wrong input to autoencoder. Please use 5 int values in a tuple.")
        return None

    # Define the encoder model
    autoencoder_input = Input(shape=(input_dim,))
    # First step of encoding
    encoder_output = Dense(layer_dims[0])(autoencoder_input)  # layers.Dense
    # Activation function layers.LeakyReLU
    encoder_output = LeakyReLU()(encoder_output)
    # Second step (latent space)
    encoder_output = Dense(layer_dims[1])(encoder_output)
    encoder_output = LeakyReLU()(encoder_output)
    # Encoder model
    encoder = Model(autoencoder_input, encoder_output)

    # Define the decoder model
    decoder_input = Input(shape=(layer_dims[1],))
    decoder_output = Dense(layer_dims[2])(decoder_input)
    decoder_output = LeakyReLU()(decoder_output)
    decoder_output = Dense(input_dim, activation='sigmoid')(decoder_output)
    # Decoder model
    decoder = Model(decoder_input, decoder_output)

    # Define the autoencoder model
    autoencoder_input = Input(shape=(input_dim,))
    autoencoder_output = decoder(encoder(autoencoder_input))
    autoencoder = Model(autoencoder_input, autoencoder_output)

    return autoencoder, encoder, decoder
