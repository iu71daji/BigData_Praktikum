import os
import sys
from loompy import connect
from pandas import DataFrame
from random import sample
from os import listdir
import numpy as np
import pickle


def check_make_path(directory: str):
    cwd = os.getcwd()
    path_to_dir = os.path.join(cwd, directory)
    # check if folder exists
    if not os.path.exists(path_to_dir):
        os.makedirs(path_to_dir)
    print(path_to_dir)
    return path_to_dir


def get_list_of_loom_files(directory="data/", file_names_only=True):
    """
    Returns a list of all loom-files in directory
    (only file names by default)
    :param file_names_only: True by default, if False then:
            f"{directory}/{file}" will be used
    :param directory: name of the local directory that will be looked through
    :return: list of loom files in directory
    """
    loom_file_list = list()
    for file in listdir(directory):
        if file[-5:] == ".loom":
            if file_names_only:
                loom_file_list.append(file)
            else:
                loom_file_list.append(f"{directory}{file}")
    return loom_file_list


def check_data_existence(file_name: str, directory: str):
    """
    :param file_name: the name of the file to be checked
    :param directory: name of local directory in which the file lives
    :return: True if it exists, False otherwise
    """
    if directory not in listdir():
        print(f"WARNING: {directory} is not a local directory!")
        return False
    if file_name[-5:] != ".loom":
        print(f"WARNING: {file_name} is no valid file name. Only use '*.loom'!")
        return False
    if file_name not in get_list_of_loom_files(directory):
        print(f"WARNING: {file_name} doesn't exist in {directory}")
        return False
    # Checks OK
    return True


def sample_of_several_looms(loom_list: list, nr_cols: int or None,
                            nr_rows: int or None = None,
                            directory="data", check_attrs=False):
    """
    Like loom_to_numpy_n_tags - but for several loom files.
    Draws the same (randomly chosen) genes from several loom files.
    :param loom_list: list of loom files
    :param directory: name of local directory in which the file lives
    :param nr_cols: limits the number of attributes (columns) to be read.
    :param check_attrs: if True, the function will check whether all files
        share the same attribute labels (is slow). Only if nr_cols is set!
    :param nr_rows: to limit the number ob objects (per loom file)
    :return: data, object_labels, attribute_labels
    """
    # The random sample is to be chosen just once!
    # Because we want to have the same attributes from all loom files.
    rnd_sample_cols = None
    # The attribute names should be common to all
    common_attrs = None
    # List of all objects
    object_labels = None
    # Data
    data = None

    for loom in loom_list:
        # CHECKS #
        if not check_data_existence(loom, directory):
            return None

        file = f"{directory}/{loom}"
        organ_label = loom.split("__")[0]

        print(f"Sample data from file {file}")
        with connect(file) as ds:
            # If nr_rows is not limited:
            if nr_rows is None or nr_rows >= ds.shape[1]:  # Take all objects
                nr_rows = ds.shape[1]  # Beware: columns in ds are rows in df
            if nr_cols is None or nr_cols >= ds.shape[0]:  # Take all features
                nr_cols = ds.shape[0]  # Beware: columns in ds are rows in df
                # Use all attributes
                if data is None:
                    data = ds[:, :nr_rows].transpose()  # Beware ^T
                    common_attrs = ds.ra.ensembl_ids[:nr_cols]  # Beware ^T
                else:
                    new_data = ds[:, :nr_rows].transpose()
                    # Concatenate the data
                    data = np.concatenate((data, new_data), axis=0)
            else:  # nr_cols is not None => sample
                if rnd_sample_cols is None:
                    # Choose nr_cols many indices
                    rnd_sample_cols = sample(range(ds.shape[0]), nr_cols)
                    rnd_sample_cols = sorted(rnd_sample_cols)
                    common_attrs = ds.ra.ensembl_ids[rnd_sample_cols]  # ! ^T
                    data = ds[rnd_sample_cols, :nr_rows].transpose()  # ! ^T
                else:
                    new_data = ds[rnd_sample_cols, :nr_rows].transpose()
                    # Concatenate the data
                    data = np.concatenate((data, new_data), axis=0)
                if check_attrs:
                    attrs_labels = ds.ra.ensembl_ids[
                        rnd_sample_cols]  # Beware ^T
                    for i in range(nr_cols):
                        if not attrs_labels[i] == common_attrs[i]:
                            print(f"WARNING: no common attributes for {file}.",
                                  f"\nAttributes are:\n{attrs_labels}\n",
                                  f"Attributes should be:\n{common_attrs}")
                            break
            # Object labels
            if object_labels is None:
                object_lbs = ds.ca.CellID[:nr_rows]
                # Add organ-label to object_labels
                object_labels = [f"{organ_label}__{x}" for x in object_lbs]
            else:
                new_o_lbs = ds.ca.CellID[:nr_rows]  # Get new labels
                # Add organ-label to new object_labels
                new_o_lbs = [f"{organ_label}__{x}" for x in new_o_lbs]
                # Add them to the list of all labels
                object_labels = np.append(object_labels, new_o_lbs)
    if check_attrs:
        # Test for duplicates in object_labels and attributes
        if len(set(object_labels)) != len(object_labels):
            print("There are duplicates in the object-labels!")
        if len(set(common_attrs)) != len(common_attrs):
            print("There are duplicates in the attribute-labels!")
    return data, object_labels, common_attrs


def inspect_loom_file(file_name: str, q=20, calc_mean=False, verbose=False):
    """
    Prints info about the loom-file
    :param q: number of attribute-entries being printed (first q entries)
    :param file_name: the name of the loom-file (without path!)
    :param calc_mean: if False, it only prints row- and col-attributes and shape
    :param verbose: if True, it prints more information
    """
    if file_name[-5:] == ".loom":
        print(f"\n##### FILE INFO #####    (On the file: {file_name})")
        with connect(f"data/{file_name}") as ds:
            print("shape:", ds.shape)
            print("### Column attributes:\n    ", *ds.ca, sep="   ")
            print("### Row attributes:\n    ", *ds.ra, sep="   ")
            if verbose:
                print("\n### COLUMN ###\n##############\n")
                for col_attr in ds.ca:
                    print(f"\n{col_attr} (first {q}):\n", *ds.ca[col_attr][:q])
                print("\n### ROW ###\n###########\n")
                for row_attr in ds.ra:
                    print(f"\n{row_attr} (first {q}):\n", *ds.ra[row_attr][:q])
                    # r_a_ser = pd.Series(ds.ra[row_attr])
                    # dups = r_a_ser.duplicated()
                    # print("*** number of duplicates:", dups.sum(), "\nwith",
                    #       r_a_ser[dups])
            if verbose:
                print("\n### DATA ###\n############\n")
                if ds.shape[0] < q:
                    if ds.shape[1] < q:
                        print(ds[:, :])
                    else:
                        print(ds[:, :q])
                elif ds.shape[1] < q:
                    if ds.shape[0] < q:
                        print(ds[:, :])
                    else:
                        print(ds[:q, :])
                else:
                    print(ds[:q, :q])
            if not calc_mean:  # Stop
                return
            # Means of first q objects
            means_q = []
            lower = 0
            print("Processing data..")
            for upper in range(25, ds.shape[0], 25):
                sys.stdout.write(f"\033[K  {upper} from {ds.shape[0]}\r")
                means_q += list(np.mean(ds[lower:upper, :], axis=0))
                lower = upper
            means_q += list(np.mean(ds[lower:ds.shape[0], :], axis=0))
            sys.stdout.write(f"\033[K  {upper} from {ds.shape[0]}\r")
            print(f"\nThe first {q} means of objects (axis=0):\n", means_q[:q])
            print("Mean of these means:", np.mean(means_q))


def random_sample(df: DataFrame, nr_cols: int, nr_rows: int = None):
    """
    Returns a random sample DataFrame of the input DataFrame df
    :param df: input DataFrame
    :param nr_rows: number of rows (objects) to be kept
    :param nr_cols: number of columns (attributes) to be kept
    :return: the sample DataFrame
    """
    # Take a random selection of columns (features)
    rand_sample_cols = sample(range(df.shape[1]), nr_cols)
    if nr_rows is not None:  # Take also random selection of rows (objects)
        rand_sample_rows = sample(range(df.shape[0]), nr_rows)
        return df.iloc[rand_sample_rows, rand_sample_cols]
    else:
        # Return a DataFrame with all objects but just a sample of attributes
        return df.iloc[:, rand_sample_cols]


def get_data_labels(df: DataFrame):
    obj_names = df.index
    labels = [x.split("__")[0] for x in obj_names]
    return labels


def save_df(df, name: str, directory="testfiles"):
    file_path = os.path.join(directory, name)
    with open(f"{file_path}.pkl", "wb") as outp:
        pickle.dump(df, outp, pickle.HIGHEST_PROTOCOL)
    print(f"DataFrame {directory}{name} created and saved with pickle")


def load_df(name: str, directory="testfiles"):
    file_path = os.path.join(directory, name)
    with open(f"{file_path}.pkl", "rb") as inp:
        df = pickle.load(inp)
    return df
